package gob.hidalgo.curso.components.generales;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.administracion.UsuarioEO;
import gob.hidalgo.curso.database.generales.ClienteEO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("ClientesC")
public class ClientesC {

	@Autowired 
	private SqlSession sqlSession;
	
	@Autowired
	private MensajesC mensajesC;
	
	public ClientesC() {
		super();
		log.debug("Se crea componente ClientesC");
	}
	
	public Modelo<ClienteEO> modelo(){
		List<ClienteEO> listado;
		listado = sqlSession.selectList("generales.clientes.listado");
		return new Modelo<ClienteEO>(listado);
	}
	
	public ClienteEO nuevo() {
		return new ClienteEO();
	}
	
	public boolean guardar(ClienteEO cliente) {
		if(cliente.getId() == null) {
			sqlSession.insert("generales.clientes.insertar", cliente);
			mensajesC.mensajeInfo("Cliente agregado exitosamente");
		} else {
			sqlSession.update("generales.clientes.actualizar", cliente); 
			mensajesC.mensajeInfo("Cliente actualizado exitosamente");
		}
		return true;
	}
	
	public boolean eliminar(ClienteEO cliente) {
		sqlSession.insert("generales.clientes.eliminar", cliente);
		mensajesC.mensajeInfo("Cliente eliminado exitosamente");
		return true;
	}
	
}
